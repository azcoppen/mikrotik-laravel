# MikroTik RouterOS SDK for Laravel

```
composer require smartrooms/mikrotik-laravel
```

```
$config = [
    'host' => '129.168.0.1',
    'username' => 'admin',
    'password' => '12345',
];
```

## Get all network interfaces
```
use MikroTik\RouterOS\Commands\Network\Interfaces;

$interfaces = app (Interfaces::class)->config($config)->response();
```

## Get all IP addresses

```
use MikroTik\RouterOS\Commands\IP\Address;

$addresses = app (Address::class)->config($config)->all()->read()->response();
```

## Get all security profiles

```
use MikroTik\RouterOS\Commands\IP\SecurityProfile;

$profiles = app (SecurityProfile::class)->config($config)->all()->read()->response();
```

## Get a single security profile

```
use MikroTik\RouterOS\Commands\Wireless\SecurityProfile;

$network = app (SecurityProfile::class)->config($config)->show(1)->read()->response();
```

## Get all wireless networks

```
use MikroTik\RouterOS\Commands\Wireless\Network;

$networks = app (Network::class)->config($config)->all()->read()->response();
```

## Get a single wireless network

```
use MikroTik\RouterOS\Commands\Wireless\Network;

$network = app (Network::class)->config($config)->show(1)->read()->response();
```

## Create a Wifi network

```
use MikroTik\RouterOS\Commands\Wireless\SecurityProfile;
use MikroTik\RouterOS\Commands\Wireless\Network;

$profile    = app (SecurityProfile::class)->config($config)->create ('new-profile', 'crappypasswprd')->response();
$network    = app (Network::class)->config($config)->create ('wlan1', 'my-network-name', 'new-profile')->response();
```

## Change the network name

```
use MikroTik\RouterOS\Commands\Wireless\Network;

$result = app (Network::class)->config($config)->set (1, 'ssid', 'new-network-name')->response();
```

Any from here: https://wiki.mikrotik.com/wiki/Manual:Interface/Wireless#General_interface_properties


## Change the network password

```
use MikroTik\RouterOS\Commands\Wireless\SecurityProfile;

$result = app (SecurityProfile::class)->config($config)->set (1, 'wpa-pre-shared-key', 'newpassword')->response();
$result = app (SecurityProfile::class)->config($config)->set (1, 'wpa2-pre-shared-key', 'newpassword')->response();
```

Any from here: https://wiki.mikrotik.com/wiki/Manual:Interface/Wireless#WPA_properties

## Remove a network

```
use MikroTik\RouterOS\Commands\Wireless\Network;

$result = app (Network::class)->config($config)->remove (1)->response();
```

## Remove a security profile

```
use MikroTik\RouterOS\Commands\Wireless\SecurityProfile;

$result = app (SecurityProfile::class)->config($config)->remove (1)->response();
```

## Open a port

```
use MikroTik\RouterOS\Commands\IP\Firewall\Port;

$port = app (Port::class)->config($config)->open (9200, 'tcp', '0.0.0.0', 9200)->response();
```

## Close a port

```
use MikroTik\RouterOS\Commands\IP\Firewall\Port;

$port = app (Port::class)->config($config)->close (9200, 'tcp')->response();
```
