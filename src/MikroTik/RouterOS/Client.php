<?php

namespace MikroTik\RouterOS;

use \RouterOS\Config;
use \RouterOS\Client AS ROSClient;

abstract class Client
{
    public $host;
    public $port;
    public $username;
    public $password;
    public $ssl;
    public $legacy;
    public $timeout;
    public $attempts;
    public $delay;

    public $config;
    public $connection;
    public $query;
    public $sentence;
    public $response;

    public function config ( array $config )
    {
        $this->host         = $config['host'] ?? '192.168.1.3';
        $this->port         = $config['port'] ?? 8728;
        $this->username     = $config['username'] ?? 'admin';
        $this->password     = $config['password'] ?? 'admin';
        $this->ssl          = $config['ssl'] ?? false;
        $this->legacy       = $config['legacy'] ?? false;
        $this->timeout      = $config['timeout'] ?? 10;
        $this->attempts     = $config['attempts'] ?? 10;
        $this->delay        = $config['delay'] ?? 1;

        return $this;
    }

    public function connect ()
    {
        $this->config = new Config ([
            'host'      => $this->host,
            'port'      => $this->port,
            'user'      => $this->username,
            'pass'      => $this->password,
            'ssl'       => $this->ssl,
            'legacy'    => $this->legacy,
            'timeout'   => $this->timeout,
            'attempts'  => $this->attempts,
            'delay'     => $this->delay,
        ]);

        $this->client = new ROSClient ($this->config);

        return $this;
    }

}
