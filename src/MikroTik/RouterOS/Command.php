<?php

namespace MikroTik\RouterOS;

use \RouterOS\Query;
use \Exception;

use MikroTik\RouterOS\Exceptions\ClientException;
use MikroTik\RouterOS\Exceptions\InvalidSentenceException;
use MikroTik\RouterOS\Exceptions\CommandFailedException;

abstract class Command extends Client
{
    public $id;
    public $wheres = [];
    public $params = [];

    private function __check_response () : void
    {
        if ( $this->response && is_array ($this->response) )
        {
            if ( array_key_exists ('after', $this->response) )
            {
                if (array_key_exists ('message', $this->response['after']) )
                {
                    throw new CommandFailedException ($this->response['after']['message']);
                }

                if (array_key_exists ('ret', $this->response['after']) )
                {
                    $this->id = $this->response['after']['ret'];
                }
            }
        }
    }

    public function param ( string $attribute, $value ) : self
    {
        $this->params[$attribute] = $value;
        return $this;
    }

    public function where ( string $attribute, $value ) : self
    {
        $this->wheres[$attribute] = $value;
        return $this;
    }

    public function read (string $slice = null)
    {
        $this->connect();

        if ( !$this->client )
        {
            throw new ClientException ("Client connection available.");
        }

        if ( !$this->sentence || empty ($this->sentence) )
        {
            throw new InvalidSentenceException ("No command string (sentence) to send to device.");
        }

        $this->response = $this->client->query($this->sentence)->read();

        if ( $slice !== FALSE && is_numeric ($slice) )
        {
            foreach ($this->response AS $item)
            {
                if ( $item['.id'] === '*'.$slice )
                {
                    $this->response = $item;
                    return $this;
                }
            }

            $this->response = [];
        }

        $this->__check_response ();

        return $this;
    }

    public function write ()
    {
        $this->connect();

        if ( !$this->client )
        {
            throw new ClientException ("Client connection available.");
        }

        if ( !$this->sentence || empty ($this->sentence) )
        {
            throw new InvalidSentenceException ("No command string (sentence) to send to device.");
        }

        $this->query = new Query ($this->sentence);

        if ( !count ($this->wheres) && !count ($this->params) )
        {
            $this->response = $this->client->query ($this->query)->read();
            $this->__check_response ();
        }

        if ( count ($this->wheres) )
        {
            foreach ( $this->wheres AS $name => $value )
            {
                $this->query = $this->query->where ($name, $value);
            }
        }

        if ( count ($this->params) )
        {
            foreach ( $this->params AS $name => $value )
            {
                $this->query = $this->query->equal ($name, $value);
            }
        }

        $this->response = $this->client->query ($this->query)->read();
        $this->__check_response ();

        return $this;
    }

    public function response ()
    {
        return $this->response;
    }
}
