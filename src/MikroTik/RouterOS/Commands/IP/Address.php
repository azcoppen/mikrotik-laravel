<?php

namespace MikroTik\RouterOS\Commands\IP;

use MikroTik\RouterOS\Command;
use MikroTik\RouterOS\Contracts\RouterOSCommandContract;

class Address extends Command implements RouterOSCommandContract
{
    public $sentence = '/ip/address/print';
}
