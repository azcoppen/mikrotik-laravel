<?php

namespace MikroTik\RouterOS\Commands\IP\DHCP;

use MikroTik\RouterOS\Command;
use MikroTik\RouterOS\Contracts\RouterOSCommandContract;

class Lease extends Command implements RouterOSCommandContract
{
    public $sentence = '/ip/dhcp-server/lease/print';
}
