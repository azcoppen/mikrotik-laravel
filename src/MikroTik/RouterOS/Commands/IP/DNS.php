<?php

namespace MikroTik\RouterOS\Commands\IP;

use MikroTik\RouterOS\Command;
use MikroTik\RouterOS\Contracts\RouterOSCommandContract;

class DNS extends Command implements RouterOSCommandContract
{
    public $sentence = '/ip/dns/print';
}
