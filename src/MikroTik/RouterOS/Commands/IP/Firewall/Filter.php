<?php

namespace MikroTik\RouterOS\Commands\IP\Firewall;

use MikroTik\RouterOS\Command;
use MikroTik\RouterOS\Contracts\RouterOSCommandContract;

class Filter extends Command implements RouterOSCommandContract
{
    public $sentence = '/ip/firewall/filter';

    //https://wiki.mikrotik.com/wiki/Manual:IP/Firewall/Filter#Properties
    public $options = [
        'chain'             => 'forward',
        'action'            => 'drop',
        'protocol'          => '',
        'time'              => '',
        'src-mac-address'   => '',
    ];

    public function add ( string $destination_port, string $mac_address, string $time = null )
    {
        $this->sentence += '/add';

        $this->options['src-mac-address']    = $mac_address;
        $this->options['time']               = $time;

        if ( !$time )
        {
            unset($this->options['time']);
        }

        $this->write ($this->options);
        
        return $this;

    }
}
