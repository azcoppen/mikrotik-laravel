<?php

namespace MikroTik\RouterOS\Commands\IP\Firewall;

use MikroTik\RouterOS\Command;
use MikroTik\RouterOS\Contracts\RouterOSCommandContract;

class Port extends Command implements RouterOSCommandContract
{
    public $sentence = '/ip/firewall/nat';

    // https://wiki.mikrotik.com/wiki/Manual:IP/Firewall/NAT#Properties
    public $options = [
        'chain'             => 'dstnat',
        'dst-port'          => '',
        'action'            => 'dst-nat',
        'protocol'          => '',
        'to-address'        => '',
        'to-port'           => '',
    ];

    public function open ( string $destination_port, string $protocol = 'tcp', string $to_address, string $to_port ) : self
    {
        $this->sentence .= '/add';

        $this->options['dst-port']      = $destination_port;
        $this->options['protocol']      = $protocol;
        $this->options['to-address']    = $to_address;
        $this->options['to-port']       = $to_port;

        foreach ($this->options AS $name => $value)
        {
            $this->param ($name, $value);
        }

        $this->write();

        return $this;
    }

    public function close ( string $destination_port, string $protocol = 'tcp', string $to_address, string $to_port ) : self
    {

    }
}
