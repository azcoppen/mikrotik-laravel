<?php

namespace MikroTik\RouterOS\Commands\Network;

use MikroTik\RouterOS\Command;
use MikroTik\RouterOS\Contracts\RouterOSCommandContract;

class Interfaces extends Command implements RouterOSCommandContract
{
    public $sentence = '/interface/print';
}
