<?php

namespace MikroTik\RouterOS\Commands\Network;

use MikroTik\RouterOS\Command;
use MikroTik\RouterOS\Contracts\RouterOSCommandContract;

class TrafficMonitor extends Command implements RouterOSCommandContract
{
    public $sentence = '/interface/monitor-traffic/wlan1';
}
