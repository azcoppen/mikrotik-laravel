<?php

namespace MikroTik\RouterOS\Commands\System;

use MikroTik\RouterOS\Command;
use MikroTik\RouterOS\Contracts\RouterOSCommandContract;

class Clock extends Command implements RouterOSCommandContract
{
    public $sentence = '/system/clock/print';
}
