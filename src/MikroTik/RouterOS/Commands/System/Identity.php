<?php

namespace MikroTik\RouterOS\Commands\System;

use MikroTik\RouterOS\Command;
use MikroTik\RouterOS\Contracts\RouterOSCommandContract;

class Identity extends Command implements RouterOSCommandContract
{
    public $sentence = '/system/identity/print';
}
