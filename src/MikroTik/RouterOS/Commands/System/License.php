<?php

namespace MikroTik\RouterOS\Commands\System;

use MikroTik\RouterOS\Command;
use MikroTik\RouterOS\Contracts\RouterOSCommandContract;

class License extends Command implements RouterOSCommandContract
{
    public $sentence = '/system/license/print';
}
