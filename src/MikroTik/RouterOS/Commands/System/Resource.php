<?php

namespace MikroTik\RouterOS\Commands\System;

use MikroTik\RouterOS\Command;
use MikroTik\RouterOS\Contracts\RouterOSCommandContract;

class Resource extends Command implements RouterOSCommandContract
{
    public $sentence = '/system/resource/print';
}
