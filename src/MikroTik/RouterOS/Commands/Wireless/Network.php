<?php

namespace MikroTik\RouterOS\Commands\Wireless;

use MikroTik\RouterOS\Command;
use MikroTik\RouterOS\Contracts\RouterOSCommandContract;

class Network extends Command implements RouterOSCommandContract
{
    public $interface;
    public $ssid;
    public $profile;
    public $sentence = '/interface/wireless';

    // https://wiki.mikrotik.com/wiki/Manual:Interface/Wireless#General_interface_properties
    public $options = [
        'mode'              => 'ap-bridge',
        'master-interface'  => '',
        'ssid'              => '',
        'security-profile'  => '',
        'disabled'          => 'no',
    ];

    public function all () : self
    {
        $this->sentence .= '/print';
        $this->read();
        return $this;
    }

    public function show ( string $id ) : self
    {
        $this->sentence .= '/print';
        $this->read($id);
        return $this;
    }

    public function create ( string $interface = 'wlan1', string $ssid, string $profile = 'default' ) : self
    {
        $this->sentence .= '/add';

        $this->options['master-interface'] = $interface;
        $this->options['ssid']             = $ssid;
        $this->options['security-profile'] = $profile;

        foreach ($this->options AS $name => $value)
        {
            $this->param ($name, $value);
        }

        $this->write ();

        return $this;
    }

    public function set ( string $id, string $attribute, string $value ) : self
    {
        $this->sentence .= '/set';
        $this
            ->param ('.id', '*'.$id)
            ->param ($attribute, $value)
            ->write();
        return $this;
    }

    public function remove ( string $id ) : self
    {
        $this->sentence .= '/remove';
        $this
            ->param ('.id', '*'.$id)
            ->write();
        return $this;
    }
}
