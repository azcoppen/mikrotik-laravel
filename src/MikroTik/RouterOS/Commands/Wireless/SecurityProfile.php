<?php

namespace MikroTik\RouterOS\Commands\Wireless;

use MikroTik\RouterOS\Command;
use MikroTik\RouterOS\Contracts\RouterOSCommandContract;

use \Exception;

class SecurityProfile extends Command implements RouterOSCommandContract
{
    public $name;
    public $password;
    public $sentence = '/interface/wireless/security-profiles';

    //https://wiki.mikrotik.com/wiki/Manual:Interface/Wireless#WPA_properties
    public $options = [
        'mode'                  => 'dynamic-keys',
        'authentication-types'  => 'wpa2-psk',
    ];

    public function all () : self
    {
        $this->sentence .= '/print';
        $this->read();
        return $this;
    }

    public function show ( string $id ) : self
    {
        $this->sentence .= '/print';
        $this->read($id);
        return $this;
    }

    public function create ( string $name, string $password ) : self
    {
        if ( strlen($password) < 8 )
        {
            throw new Exception ("Password for WPA2-PSK needs to be at least 8 characters.");
        }

        $this->sentence .= '/add';

        $this->options['name']                  = $name;
        $this->options['wpa-pre-shared-key']    = $password;
        $this->options['wpa2-pre-shared-key']   = $password;

        foreach ($this->options AS $name => $value)
        {
            $this->param ($name, $value);
        }

        $this->write ();

        return $this;
    }

    public function set ( string $id, string $attribute, string $value ) : self
    {
        $this->sentence .= '/set';
        $this
            ->param ('.id', '*'.$id)
            ->param ($attribute, $value)
            ->write();
        return $this;
    }

    public function remove ( string $id ) : self
    {
        $this->sentence .= '/remove';
        $this
            ->param ('.id', '*'.$id)
            ->write();
        return $this;
    }
}
