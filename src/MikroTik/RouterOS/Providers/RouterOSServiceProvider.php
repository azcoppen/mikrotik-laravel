<?php

namespace MikroTik\RouterOS\Providers;

use Illuminate\Support\ServiceProvider;

class RouterOSServiceProvider extends ServiceProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot ()
    {

    }

    public function provides()
    {
        return [

        ];
    }
}
